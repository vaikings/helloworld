//
//  VBTPlanetMatcher.h
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/11/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import <Expecta/Expecta.h>

EXPMatcherInterface(isKindOfPlanet, (void));

#define isKindOfPlanet isKindOfPlanet
