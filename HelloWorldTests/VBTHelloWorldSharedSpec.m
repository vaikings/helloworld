//
//  VBTHelloWorldSharedSpec.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/11/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import <Specta/Specta.h>
#define EXP_SHORTHAND
#import "Expecta.h"

//Globally shared examples 

SharedExamplesBegin(VBTHelloWorldShared)
sharedExamplesFor(@"TableView Datasource", ^(NSDictionary *data) {
    describe(@"datasource", ^{
        it(@"conforms to UITableViewDataSource protocol", ^{
            UIViewController* controller = data[@"datasource"];
            expect(controller).to.conformTo(@protocol(UITableViewDataSource));
        });
    });
});

SharedExamplesEnd

