//
//  VBTPlanetSpec.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/3/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "Specta.h"

#define EXP_SHORTHAND
#import "Expecta.h"

#import "VBTPlanet.h"
#import "ExpMatcher+isKindOfPlanet.h"

SpecBegin(VBTPlanet)

describe(@"Planet", ^{
    __block VBTPlanet *myPlanet ;
    
    //locally shared examples
    sharedExamplesFor(@"planet", ^(NSDictionary *data) {
        
        
    
    });

    beforeEach(^{
        myPlanet = [VBTPlanet planetWithName:@"Earth" distance:@1];
    });
    
    //Custom matcher example
    it(@"should be a kind of planet", ^{
        expect(myPlanet).isKindOfPlanet();
    });
    
    it(@"should be able to create a new instance of planet", ^{
        expect(myPlanet).toNot.beNil();
    });
    
    it(@"should revolve around the sun in 365 days", ^AsyncBlock {
        // Async example blocks need to invoke done() callback.
        // setAsyncSpecTimeout(NSTimeInterval timeout)  default is 10 sec
        // setAsyncSpecTimeout(365*24*60*60)
        done();
    });
    
    it(@"should have a name property", ^{
        expect(myPlanet.name).to.equal(@"Earth");
    });
    
    it(@"should have a distance property", ^{
        expect(myPlanet.distance).to.equal(@1);
    });
    
    
    it(@"should be able to compare 2 planets", ^{
        VBTPlanet* planet1 = [VBTPlanet planetWithName:@"foo" distance:@1.4];
        VBTPlanet* planet2 = [VBTPlanet planetWithName:@"foo" distance:@1.4];
        
        expect(planet1).to.equal(planet2);
    });
    
    context(@"when getting all planets", ^{
        it(@"should have 8 planets", ^{
            NSArray* allPlanets = [VBTPlanet allPlanets];
            
            expect(allPlanets.count).to.equal(8);
        });
        
        it(@"should have planets in the correct order", ^{
            NSArray* allPlanets = [VBTPlanet allPlanets];
            
            expect([(VBTPlanet*)allPlanets[0] name]).to.equal(@"Mercury");
            expect([(VBTPlanet*)allPlanets[1] name]).to.equal(@"Venus");
            expect([(VBTPlanet*)allPlanets[2] name]).to.equal(@"Earth");
            expect([(VBTPlanet*)allPlanets[3] name]).to.equal(@"Mars");
            expect([(VBTPlanet*)allPlanets[4] name]).to.equal(@"Jupiter");
            expect([(VBTPlanet*)allPlanets[5] name]).to.equal(@"Saturn");
            expect([(VBTPlanet*)allPlanets[6] name]).to.equal(@"Uranus");
            expect([(VBTPlanet*)allPlanets[7] name]).to.equal(@"Neptune");
        });

    });
});

SpecEnd