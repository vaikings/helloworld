//
//  VBTPlanetsDataSourceSpec.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/5/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "Specta.h"

#define EXP_SHORTHAND
#import "Expecta.h"
#import <OCMock/OCMock.h>

#import "VBTArrayDataSource.h"
#import "VBTPlanet.h"



SpecBegin(VBTArrayDataSource)

describe(@"planets datasource", ^{
    __block VBTArrayDataSource * datasource ;
    
    datasource = [VBTArrayDataSource withItems:nil cellIdentifier:@"" configureCellBlock:nil];
    itShouldBehaveLike(@"TableView Datasource", @{@"datasource":datasource});
    
    it(@"should have valid properties when configured", ^{
        VBTPlanet* earth = [VBTPlanet planetWithName:@"Earth" distance:@1];
        VBTPlanet* mars = [VBTPlanet planetWithName:@"Mars" distance:@1.5];
        
        datasource = [VBTArrayDataSource withItems:@[earth, mars] cellIdentifier:@"PlanetCell" configureCellBlock:^(id cell, id item) {
            UITableViewCell *aCell = (UITableViewCell *) cell;
            VBTPlanet *planet = (VBTPlanet *) item;
            aCell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@ AU)", planet.name, planet.distance];
        }];
        
        expect(datasource.configureCellBlock).toNot.beNil();
        expect(datasource.cellIdentifier).to.equal(@"PlanetCell");
        expect(datasource.items.count).to.equal(2);
    });
    
    context(@"UITableViewDataSource protocol", ^{
        
        it(@"should return number of rows in section 0", ^{
            datasource = [VBTArrayDataSource withItems:@[@"a", @"b"] cellIdentifier:nil configureCellBlock:nil];
            
            expect([datasource tableView:nil numberOfRowsInSection:0]).to.equal(2);
        });
        
        it(@"should return a tableViewCell for the given indexPath with nil configure block", ^{
            UITableViewCell* mycell = [[UITableViewCell alloc] init];
            id mockTableView = [OCMockObject mockForClass:[UITableView class]];
            [[[mockTableView stub] andReturn:mycell] dequeueReusableCellWithIdentifier:[OCMArg any] forIndexPath:[OCMArg any]];
            
            datasource = [VBTArrayDataSource withItems:@[@"a", @"b"] cellIdentifier:@"testCell" configureCellBlock:nil];
            UITableViewCell* returnedCell = [datasource tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] ;
            
            expect(returnedCell).to.equal(mycell);
        });
        
        it(@"should return a nil cell for the given indexPath for nil reuse identifier", ^{
            UITableViewCell* mycell = [[UITableViewCell alloc] init];
            id mockTableView = [OCMockObject mockForClass:[UITableView class]];
            [[[mockTableView stub] andReturn:mycell] dequeueReusableCellWithIdentifier:[OCMArg any] forIndexPath:[OCMArg any]];
            
            datasource = [VBTArrayDataSource withItems:@[@"a", @"b"] cellIdentifier:nil configureCellBlock:nil];
            UITableViewCell* returnedCell = [datasource tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] ;
            
            expect(returnedCell).to.beNil;
        });
        
        
        it(@"should return a configured cell for the given indexPath", ^{
            id mockCell = [OCMockObject mockForClass:[UITableViewCell class]];
            [[[mockCell stub] andReturn:[UILabel new]] detailTextLabel];
            id mockTableView = [OCMockObject mockForClass:[UITableView class]];
            [[[mockTableView stub] andReturn:mockCell] dequeueReusableCellWithIdentifier:[OCMArg any] forIndexPath:[OCMArg any]];
            
            VBTPlanet* earth = [VBTPlanet planetWithName:@"Earth" distance:@1];
            VBTPlanet* mars = [VBTPlanet planetWithName:@"Mars" distance:@1.5];
            datasource = [VBTArrayDataSource withItems:@[earth, mars] cellIdentifier:@"PlanetCell" configureCellBlock:^(id cell, id item) {
                UITableViewCell *aCell = (UITableViewCell *) cell;
                VBTPlanet *planet = (VBTPlanet *) item;
                aCell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@ AU)", planet.name, planet.distance];

            }];
            UITableViewCell* returnedCell = [datasource tableView:mockTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] ;
            
            expect(returnedCell.detailTextLabel.text).to.equal(@"Earth (1 AU)");
        });
    });
});


SpecEnd
