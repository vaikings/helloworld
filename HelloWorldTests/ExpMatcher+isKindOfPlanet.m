//
//  VBTPlanetMatcher.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/11/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "ExpMatcher+isKindOfPlanet.h"
#import "VBTPlanet.h"

EXPMatcherImplementationBegin(isKindOfPlanet, (void)) {
    BOOL actualIsNil = (actual == nil);
    
    prerequisite(^BOOL {
        return !(actualIsNil);
    });
    
    match(^BOOL {
        return [actual isKindOfClass:[VBTPlanet class]];
    });
    
    failureMessageForTo(^NSString * {
        if (actualIsNil)
            return @"the actual value is nil/null";
        return [NSString
                stringWithFormat:@"expected: a kind of %@, "
                "got: an instance of %@, which is not a kind of %@",
                [VBTPlanet class], [actual class], [VBTPlanet class]];
    });
    
    failureMessageForNotTo(^NSString * {
        if (actualIsNil)
            return @"the actual value is nil/null";
        return [NSString
                stringWithFormat:@"expected: not a kind of %@, "
                "got: an instance of %@, which is a kind of %@",
                [VBTPlanet class], [actual class], [VBTPlanet class]];
    });
}
EXPMatcherImplementationEnd