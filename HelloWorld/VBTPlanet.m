//
//  VBTPlanet.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/3/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "VBTPlanet.h"
@interface VBTPlanet()

@property (nonatomic, copy, readwrite) NSString* name ;
@property (nonatomic, strong, readwrite) NSNumber* distance ;

@end

@implementation VBTPlanet

+ (instancetype) planetWithName:(NSString*)name distance:(NSNumber*)distanceFromSun {
    VBTPlanet* planet = [VBTPlanet new];
    planet.name = name ;
    planet.distance = distanceFromSun ;
    
    return planet ;
}

+ (NSArray*) allPlanets
{
    VBTPlanet* mercury = [VBTPlanet planetWithName:@"Mercury" distance:@0.4];
    VBTPlanet* venus = [VBTPlanet planetWithName:@"Venus" distance:@0.7];
    VBTPlanet* earth = [VBTPlanet planetWithName:@"Earth" distance:@1.0];
    VBTPlanet* mars = [VBTPlanet planetWithName:@"Mars" distance:@1.5];
    
    VBTPlanet* jupiter = [VBTPlanet planetWithName:@"Jupiter" distance:@5.2];
    VBTPlanet* saturn = [VBTPlanet planetWithName:@"Saturn" distance:@9.5];
    VBTPlanet* uranus = [VBTPlanet planetWithName:@"Uranus" distance:@19.2];
    VBTPlanet* neptune = [VBTPlanet planetWithName:@"Neptune" distance:@30];
    
    return @[mercury, venus, earth, mars, jupiter, saturn, uranus, neptune];
}

- (BOOL)isEqualToPlanet:(VBTPlanet *)otherPlanet {
    if (!otherPlanet) {
        return NO;
    }
    
    BOOL haveEqualNames = (!self.name && !otherPlanet.name) || [self.name isEqualToString:otherPlanet.name];
    BOOL haveEqualDistance = (!self.distance && !otherPlanet.distance) || [self.distance isEqualToNumber:otherPlanet.distance];
    
    return haveEqualNames && haveEqualDistance;
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[VBTPlanet class]]) {
        return NO;
    }
    
    return [self isEqualToPlanet:(VBTPlanet *)object];
}

- (NSUInteger)hash {
    return [self.name hash] ^ [self.distance hash];
}

@end
