//
//  VBTArrayDataSource.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/5/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "VBTArrayDataSource.h"

@interface VBTArrayDataSource ()

@property (nonatomic, copy, readwrite) NSArray* items ;
@property (nonatomic, copy, readwrite) NSString* cellIdentifier ;
@property (nonatomic, copy, readwrite) TableViewCellConfigureBlock configureCellBlock ;

@end


@implementation VBTArrayDataSource

+ (instancetype) withItems:(NSArray *)anItems
     cellIdentifier:(NSString *)aCellIdentifier
 configureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock;
{
    VBTArrayDataSource * datasource = [VBTArrayDataSource new];
    datasource.items = [NSArray arrayWithArray:anItems];
    datasource.configureCellBlock = aConfigureCellBlock ;
    datasource.cellIdentifier = aCellIdentifier; 
    
    return datasource;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath; {
    return  [self.items objectAtIndex:indexPath.row] ;
}

#pragma mark - UITableViewDataSource 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier
                                                            forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    if (self.configureCellBlock != nil){
        self.configureCellBlock(cell, item);
    }
    
    return cell;
}
@end
