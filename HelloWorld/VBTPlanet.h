//
//  VBTPlanet.h
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/3/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VBTPlanet : NSObject

@property (nonatomic, copy, readonly) NSString* name ;
@property (nonatomic, strong, readonly) NSNumber* distance ;


/**
 *  Planets factory
 *
 *  @param name            name of the planet
 *  @param distanceFromSun distance from sun in AU units
 *
 *  @return instance of planet
 */
+ (instancetype) planetWithName:(NSString*)name distance:(NSNumber*)distanceFromSun ;

+(NSArray*) allPlanets ; 

@end
