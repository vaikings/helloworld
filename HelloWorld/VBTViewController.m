//
//  VBTViewController.m
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/3/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import "VBTViewController.h"
#import "VBTArrayDataSource.h"
#import "VBTPlanet.h"

static NSString* const kPlanetCell = @"PlanetCell"  ;

@interface VBTViewController ()

@property (nonatomic, strong) VBTArrayDataSource * datasource ;
@property (nonatomic, strong) UITableView* tableView ;

@end

@implementation VBTViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupTableView];
}

- (void)setupTableView
{
    CGRect tableViewFrame = CGRectOffset(self.view.bounds, 0, 20);
    
    self.tableView = [[UITableView alloc] initWithFrame:tableViewFrame];
    
    TableViewCellConfigureBlock configureCellBlock = ^(UITableViewCell* cell, VBTPlanet* item) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@ AU)", item.name, item.distance];
    } ;
    
    self.datasource = [VBTArrayDataSource withItems:[VBTPlanet allPlanets]
                                     cellIdentifier:kPlanetCell
                                 configureCellBlock:configureCellBlock];
    
    self.tableView.dataSource = self.datasource ;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kPlanetCell];
    
    [self.view addSubview:self.tableView] ;
}


@end
