//
//  VBTAppDelegate.h
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/3/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
