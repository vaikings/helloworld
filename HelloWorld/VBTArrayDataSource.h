//
//  VBTArrayDataSource.h
//  HelloWorld
//
//  Created by Vaibhav Bhatia on 3/5/14.
//  Copyright (c) 2014 Vaikings. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^TableViewCellConfigureBlock)(id cell, id item);

@interface VBTArrayDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, copy, readonly) NSArray* items ;
@property (nonatomic, copy, readonly) NSString* cellIdentifier ;
@property (nonatomic, copy, readonly) TableViewCellConfigureBlock configureCellBlock ;

+(instancetype) withItems:(NSArray*)anItems
           cellIdentifier:(NSString *)aCellIdentifier
       configureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock;


- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

@end
